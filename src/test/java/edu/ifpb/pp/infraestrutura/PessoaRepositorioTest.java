package edu.ifpb.pp.infraestrutura;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;

/**
 * @since 11/11/2014
 * @version 1.0
 * @author Ricardo Job
 */
public class PessoaRepositorioTest {

    public PessoaRepositorioTest() {
    }

    @Mock
    private PessoaRepositorio repositorio;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMock() {
        assertNull(repositorio.listarPessoas());
    }

}
