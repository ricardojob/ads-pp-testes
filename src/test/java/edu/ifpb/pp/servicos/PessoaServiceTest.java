package edu.ifpb.pp.servicos;

import edu.ifpb.pp.infraestrutura.PessoaRepositorio;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Ricardo Job Um Exemplo de teste de Integração
 */
public class PessoaServiceTest extends TestCase {

    private PessoaService service;

    @Mock
    private PessoaRepositorio repositorio;

    @Mock
    private PessoaValidador validador;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new PessoaService(repositorio, validador);
    }

    @Test
    public void testTamanho() {
        when(repositorio.tamanho()).thenReturn(1);
        int expResult = 1;
        int result = service.tamanho();
        assertEquals(expResult, result);


    }
}
