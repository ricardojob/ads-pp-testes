package edu.ifpb.pp.servicos;

import edu.ifpb.pp.Pessoa;
import static junit.framework.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Ricardo Job
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(PessoaFabrica.class)
public class PessoaFabricaTest {

    public PessoaFabricaTest() {
    }

    @Test
    public void valoresVazios() {
        Pessoa pessoa = new Pessoa("Chaves", 19);
        PowerMockito.mockStatic(PessoaFabrica.class);
        when(PessoaFabrica.create("Chaves", 19)).thenReturn(pessoa);
        assertSame(PessoaFabrica.create("Chaves", 19), pessoa);
    }
}
