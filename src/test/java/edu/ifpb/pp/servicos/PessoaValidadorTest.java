 package edu.ifpb.pp.servicos;

import edu.ifpb.pp.Pessoa;
import junit.framework.TestCase;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Ricardo Job
 */
public class PessoaValidadorTest extends TestCase {

    private PessoaValidador validador;

    @Test
    public void testValidar() {
        validador = mock(PessoaValidador.class);// new PessoaValidador();

        Pessoa pessoaMock = new Pessoa("Chaves", 18);

        when(validador.validar(pessoaMock)).thenReturn(true);

        //job: erro pois o nome esta vazio
        Pessoa pessoa = PessoaFabrica.create("", 22);
        assertFalse(validador.validar(pessoa));
        //job: erro pois o nome esta vazio
        pessoa = PessoaFabrica.create("   ", 22);
        assertFalse(validador.validar(pessoa));
        //job: erro pois a idade é menor que 18
        pessoa = PessoaFabrica.create("Job", 17);
        assertFalse(validador.validar(pessoa));
        //job: nome e idade corretos
        pessoa = PessoaFabrica.create("Chaves", 18);
        assertTrue(validador.validar(pessoa));
    }

    @Test(expected = ServiceException.class)
    public void testValidador() {
        PessoaValidador segundoValidador = mock(PessoaValidador.class);
        assertNotNull(segundoValidador);
        when(segundoValidador.validar(null)).thenReturn(false);
        doThrow(new ServiceException()).
                when(segundoValidador).
                validar(any(Pessoa.class));
        //failNotEquals("Valores Nulos", segundoValidador.validar(null), false);
    }

    @Test(expected = ServiceException.class)
    public void testValidadorException() {
        PessoaValidador segundoValidador = mock(PessoaValidador.class);
        when(segundoValidador.validar(null)).thenThrow(new ServiceException());
       // assertFalse(segundoValidador.validar(null));
    }
}
