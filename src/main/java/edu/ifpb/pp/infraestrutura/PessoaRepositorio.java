package edu.ifpb.pp.infraestrutura;

import edu.ifpb.pp.Pessoa;
import java.util.Iterator;

/**
 * @since 17/11/2014
 * @version 1.0
 * @author Ricardo Job
 */
public interface PessoaRepositorio {

    public boolean salvar(Pessoa pessoa);

    public Iterator<Pessoa> listarPessoas();

    public int tamanho();
}
