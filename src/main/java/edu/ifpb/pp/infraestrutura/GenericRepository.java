package edu.ifpb.pp.infraestrutura;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * @since 17/11/2014
 * @version 1.0
 * @author Ricardo Job
 */
public class GenericRepository<T> implements GenericDAO<T> {

    private final EntityManager entityManager;
    protected Class<T> clazz;
    private final String persistenceName = "DbUnit_Teste";

    public GenericRepository(Class<T> classz) {
        this.entityManager = Persistence.createEntityManagerFactory(persistenceName).createEntityManager();
        this.clazz = classz;
    }

    @Override
    public int tamanho() {
        Query query = entityManager.createQuery(" Select t From " + clazz.getSimpleName() + " t");
        return query.getResultList().size();
    }

    @Override
    public T find(int i) {
        return entityManager.find(clazz, i);
    }

    @Override
    public void save(T e) {
        entityManager.getTransaction().begin();
        entityManager.persist(e);
        entityManager.getTransaction().commit();
    }

}
