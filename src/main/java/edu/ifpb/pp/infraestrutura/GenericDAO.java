package edu.ifpb.pp.infraestrutura;

/**
 * @author Ricardo Job
 */
public interface GenericDAO<T> {

    public int tamanho();

    public T find(int i);

    public void save(T e);

}
