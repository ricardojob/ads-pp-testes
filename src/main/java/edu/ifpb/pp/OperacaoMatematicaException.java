package edu.ifpb.pp;

/**
 *
 * @author Ricardo Job
 */
public class OperacaoMatematicaException extends RuntimeException {

    public OperacaoMatematicaException() {
    }

    public OperacaoMatematicaException(String message) {
        super(message);
    }

}
