package edu.ifpb.pp.servicos;

import edu.ifpb.pp.Pessoa;

/**
 *
 * @author Ricardo Job
 */
public final class PessoaFabrica {

    private PessoaFabrica() {
    }

    public static Pessoa create(String nome, int idade) {
        //job: Aqui podemos diminuir a itera��o de quem vai 
        //usar e a classe a ser intanciada
        return new Pessoa(nome, idade);
    }
}
