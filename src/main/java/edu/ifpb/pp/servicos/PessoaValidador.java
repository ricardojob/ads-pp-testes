package edu.ifpb.pp.servicos;

import edu.ifpb.pp.Pessoa;

/**
 *
 * @author Ricardo Job
 */
public class PessoaValidador {

    // job: valida se o nome da pessoa  não está vazio
    // e se a idade é superio a 18 anos
    public boolean validar(Pessoa pessoa) {
        if (pessoa == null) {
            throw new RuntimeException("Valores Nulos");
        }
        return (!pessoa.getNome().trim().equals("") && pessoa.getIdade() >= 18);
    }
}
