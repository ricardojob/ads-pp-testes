package edu.ifpb.pp.servicos;

import edu.ifpb.pp.Pessoa;
import edu.ifpb.pp.infraestrutura.PessoaRepositorio;
import java.util.Iterator;

/**
 * @author Ricardo Job
 */
//job: Poderia ser uma interface, incialmente uma classe...
public class PessoaService {

    //job: repositorio responsavel por salvar
    private PessoaRepositorio repositorio;
    private PessoaValidador validador;

    public PessoaService() {
    }

    public PessoaService(PessoaRepositorio repositorio, PessoaValidador validador) {
        this.repositorio = repositorio;
        this.validador = validador;
    }

    //job: A melhor abordagem seria criar um objeto
    // para informar os motivos do erros, podem ser mais que um
    public boolean salvar(Pessoa pessoa) {
        if (validador.validar(pessoa)) {
            return repositorio.salvar(pessoa);
        }
        return false;
    }

    public Iterator<Pessoa> listarPessoas() {
        return repositorio.listarPessoas();
    }

    public int tamanho() {
        return repositorio.tamanho();
    }
}
